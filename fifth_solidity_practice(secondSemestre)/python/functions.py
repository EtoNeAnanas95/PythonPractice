import string

def secure(password: str) -> any:
    if len(password) < 12:
        return "Пароль должен быть больше 12 символов"
    elif not any(item.islower() for item in password):
        return "Пароль должен содержать прописные буквы"
    elif not any(item.isupper() for item in password):
        return "Пароль должен содержать строчные буквы"
    elif not any(item.isdigit() for item in password):
        return "Пароль должен содержать числа"
    elif not any(item in string.punctuation for item in password):
        return "Пароль должен содержать спец символы"
    else: return True