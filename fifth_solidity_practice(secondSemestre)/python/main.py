from flask import redirect, Flask, render_template, request, url_for
from functions import secure
from web3 import Web3
from web3.middleware import geth_poa_middleware
import contract_info as cf

w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))
w3.middleware_onion.inject(geth_poa_middleware, layer=0)

contract = w3.eth.contract(address=cf.CONTRACT_ADDRESS, abi=cf.ABI)
account=''
auth = False


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method=='POST':
        password = request.form.get('password')
        global account
        account = request.form.get('account')
        try:
            w3.geth.personal.unlock_account(account, password)
            global auth
            auth = True
            return redirect(url_for('home'))
        except Exception as e: 
            print(e)
            return render_template("authorization.html", error='Ошибка авторизации')
    else:
        return render_template("authorization.html")


@app.route('/registration', methods=['GET', 'POST'])
def registartion():
    if request.method=='POST':
        password = request.form.get('password')
        chek = secure(password)
        if chek==True:
            print(chek)
            account = w3.geth.personal.new_account(password)
            return render_template("registration.html", new_account=account)
        else:
            print('ну типа ты насрал ', chek)
            return render_template('registration.html', error=chek)

    else: return render_template("registration.html")


@app.route('/home', methods=['GET', 'POST'])
def home():
    if account:
        contract_balance = contract.functions.getBalance().call({
        'from': account
        })
    else: contract_balance=0
    if auth: acc_balance = w3.eth.get_balance(account)
    else: acc_balance = 0

    if request.method=="POST":
        money = int(request.form.get("money"))
        if request.form.get("form_type")=='deposit':
            try:
                contract.functions.pay().transact({
                    'from': account,
                    'value': money
                })
                return render_template('home.html', auth=auth, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly deposit money')
            except Exception as e: return render_template('home.html', auth=auth, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))
        elif request.form.get("form_type")=="withDraw":
            try:
                tx_hash = contract.functions.withDraw(money).transact({
                    'from' : account
                })
                return render_template('home.html', auth=auth, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly withDraw money')
            except Exception as e: return render_template('home.html', auth=auth, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))

    return render_template('home.html', auth=auth, contract_balance=contract_balance, acc_balance=acc_balance)


@app.route('/home/logout')
def logout():
    global auth
    auth = False
    return redirect(url_for('home'))


@app.route("/home/advertisements", methods=['GET', 'POST'])
def advertisements():
    if account:
        contract_balance = contract.functions.getBalance().call({
        'from': account
        })
    else: contract_balance=0
    if auth: acc_balance = Web3.from_wei(w3.eth.get_balance(account), 'ether')
    else: acc_balance = 0
    Advertisements = contract.functions.getAds().call({
        'from': account
    })

    print(Advertisements)
    type = {0 : "Opened", 1 : "Closed"}
    pretty_estates = []
    for advertisement in Advertisements:
        item = {
            'Owner address': advertisement[0],
            'Buyer address': advertisement[1],
            'Price': advertisement[2],
            'Id estate': advertisement[3],
            'Creatin date': advertisement[4],
            'Ad status': type[advertisement[5]],
        }
        pretty_estates.append(item)
        print(item)

    if request.method=='POST':
        form_type = request.form.get('form_type')
        if form_type == 'change':
            id_toChange = int(request.form.get('Id'))
            try:
                contract.functions.changeStatusAd(id_toChange).transact({
                    'from': account
                })
                if pretty_estates[id_toChange-1]['Ad status'] == "Opened": pretty_estates[id_toChange-1]['Ad status'] == "Closed"
                else: pretty_estates[id_toChange-1]['Ad status'] == "Opened"
                return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly change advertisement status')
            except Exception as e: 
                print(e)
                return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))
        elif form_type == "create_new":
            try:
                id_advertisement = int(request.form.get("Id"))
                price = int(request.form.get("price"))
                contract.functions.createAd(price, id_advertisement).transact({
                    'from': account
                })
                return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly created new advertisement')
            except Exception as e:
                print(e)
                return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))
        elif form_type == "by_estate":
            try:
                by_id = int(request.form.get('Id'))
                contract.functions.buyEstate(by_id).transact({
                    'from': account
                })
                return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly bought new estate')
            except Exception as e:
                print(e)
                return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))
    return render_template("action.html", WorkingObject='Advertisements', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance)


@app.route('/home/estates', methods=['GET', 'POST'])
def estates():
    if account:
        contract_balance = contract.functions.getBalance().call({
        'from': account
        })
    else: contract_balance=0
    if auth: acc_balance = Web3.from_wei(w3.eth.get_balance(account), 'ether')
    else: acc_balance = 0
    Estates = contract.functions.getEstates().call({
        'from': account
    })
    type = {0 : "House", 1 : "Flat", 2 : "Loft"}
    pretty_estates = []
    for estate in Estates:
        item = {
            'Square': estate[0],
            'Address': estate[1],
            'Owner': estate[2],
            'Type': type[estate[3]],
            'Is active?': estate[4],
            'Estate id': estate[5],
        }
        pretty_estates.append(item)
        print(item)

    if request.method=='POST':
        form_type = request.form.get('form_type')
        if form_type == 'change':
            id_toChange = int(request.form.get('Id'))
            try:
                contract.functions.changeStatusEstate(id_toChange).transact({
                    'from': account
                })
                pretty_estates[id_toChange-1]['Is active?'] = not pretty_estates[id_toChange-1]['Is active?']
                return render_template("action.html", WorkingObject='Estates', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly change estate status')
            except Exception as e: 
                print(e)
                return render_template("action.html", WorkingObject='Estates', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))
        elif form_type == "create_new":
            try:
                size = int(request.form.get("square"))
                address = request.form.get("address")
                typeEst = int(request.form.get("type"))
                contract.functions.cretaeEstate(size, address, typeEst).transact({
                    'from': account
                })
                return render_template("action.html", WorkingObject='Estates', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, alert_message='You are succesffly created new estate')
            except Exception as e:
                print(e)
                return render_template("action.html", WorkingObject='Estates', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance, error=str(e).split(',')[0].replace("(", "").replace("'", ""))
    return render_template("action.html", WorkingObject='Estates', items=pretty_estates, contract_balance=contract_balance, acc_balance=acc_balance)


@app.errorhandler(404)
def not_found(error):
    return redirect(url_for("home"))


if __name__ == '__main__':
    app.run(debug=True)