import string
from os import system
from typing import Dict

from web3 import Web3
from web3.middleware import geth_poa_middleware

import contract_info as cf
from modules import *

w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))
w3.middleware_onion.inject(geth_poa_middleware, layer=0)

contract = w3.eth.contract(address=cf.CONTRACT_ADDRESS, abi=cf.ABI)


def secure(password: str) -> bool:
    if len(password) < 12:
        prettyOut(f"{RED}Пароль должен быть больше 12 символов{RESET}")
        return False

    if not any(item.islower() for item in password):
        prettyOut(f"{RED}Пароль должен содержать прописные буквы{RESET}")
        return False

    if not any(item.isupper() for item in password):
        prettyOut(f"{RED}Пароль должен содержать строчные буквы{RESET}")
        return False

    if not any(item.isdigit() for item in password):
        prettyOut(f"{RED}Пароль должен содержать числа{RESET}")
        return False

    if not any(item in string.punctuation for item in password):
        prettyOut(f"{RED}Пароль должен содержать спец символы{RESET}")
        return False

    return True


def register() -> None:
    system('cls')
    prettyOut(f"{YELLOW}Введите пароль: {RESET}")
    password = input('\t')
    if secure(password):
        account = w3.geth.personal.new_account(password)
        prettyOut(f"{GREEN}Ваш публичный ключ:{RESET} {account}")
    else:
        prettyOut(f"{RED}Пароль не соответствует требованиям{RESET}")


def login() -> None:
    system('cls')
    prettyOut(f"{YELLOW}Введите ваш публичный ключ:{RESET}")
    public_key = input('\t')
    prettyOut(f"{YELLOW}Введите пароль:{RESET}")
    password = input('\t')
    try:
        w3.geth.personal.unlock_account(public_key, password)
        prettyOut(f"{GREEN}\nАвторизация прошла успешно!{RESET}\n")
        return public_key
    except Exception as e:
        prettyOut(f"{RED}\nОшибка авторизации: {e}{RESET}\n")
        return ''
    finally:
        sleep(1)


def withdraw(data: dict) -> None:
    account = data['account']
    try:
        value = int(input("Введите кол-во WEI для вывода: "))
        tx_hash = contract.functions.withDraw(value).transact({
            'from': account
        })
        prettyOut(f"{GREEN}Ваши средства успешно выведены!{RESET}\n {YELLOW}Хэш транзакции: {tx_hash.hex()}{RESET}")
    except Exception as e:
        prettyOut(f"{RED}Ошибка вывода WEI: {e}{RESET}")
        input()


def createEstate(data: dict) -> None:
    system('cls')
    account = data['account']
    try:
        size = int(input(f"{YELLOW}Введите размер недвижки: {RESET}"))
        address = input(f"{YELLOW}Введите адрес недвижки: {RESET}")
        typeEst = int(input(f"{YELLOW}Введите тип недвижки: {RESET}"))
        contract.functions.cretaeEstate(size, address, typeEst).transact({
            'from': account
        })
        prettyOut(f"\n{GREEN}Недвижка успешно создана!{RESET}")
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def createAd(data: dict) -> None:
    system('cls')
    account = data['account']
    try:
        price = int(input(f"{YELLOW}Введите цену недвижки: {RESET}"))
        idEstate = int(input(f"{YELLOW}Введите айди недвижки: {RESET}"))
        contract.functions.createAd(price, idEstate).transact({
            'from': account
        })
        prettyOut(f"\n{GREEN}Объявление успешно создано!{RESET}")
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def changeEstateStatus(data: dict) -> None:
    system('cls')
    account = data['account']
    try:
        prettyOut(f'{YELLOW}Введите айди недвижки, которую хотите поменять: {RESET}')
        estateId = int(input())
        contract.functions.changeStatusEstate(estateId).transact({
            'from': account
        })
        prettyOut(f"\n{GREEN}Статус успешно изменён!{RESET}")
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def changeAdStatus(data: dict) -> None:
    system('cls')
    account = data['account']
    try:
        prettyOut(f'{YELLOW}Введите айди объявления, которое хотите поменять: {RESET}')
        estateId = int(input())
        contract.functions.changeStatusAd(estateId).transact({
            'from': account
        })
        prettyOut(f"\n{GREEN}Статус успешно изменён!{RESET}")
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def byEstate(data: dict) -> None:
    system('cls')
    account = data['account']
    try:
        prettyOut(f'{YELLOW}Введите айди недвижки, которую хотите купить: {RESET}')
        estateId = int(input())
        contract.functions.buyEstate(estateId).transact({
            'from': account
        })
        prettyOut(f"\n{GREEN}Статус успешно изменён!{RESET}")
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def enabledEstate(account: str) -> None:
    system('cls')
    try:
        Estates = contract.functions.getEstates().call({
            'from': account
        })
        prettyOut(f'{CYAN}Доступная недвижимость:{RESET}')
        iterator = 1
        for estate in Estates:
            prettyOut(f"{GREEN}{iterator}.{RESET} {estate}")
            iterator += 1
        input()
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def Advertisements(account: str) -> None:
    system('cls')
    try:
        ads = contract.functions.getAds().call({
            'from': account
        })
        prettyOut(f'{CYAN}Доступная объявления:{RESET}')
        iterator = 1
        for ad in ads:
            prettyOut(f"{GREEN}{iterator}.{RESET} {ad}")
            iterator += 1
        input()
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def getSmartContractBalance(account: str) -> None:
    system('cls')
    try:
        prettyOut(f"{GREEN}Ваш баланас на контракте:{RESET} {Web3.from_wei(w3.eth.get_balance(account), 'ether')}")
        input()
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def getAccBalance(account: str) -> None:
    system('cls')
    try:
        balance = contract.functions.getBalance().call({
            'from': account
        })
        prettyOut(f"{GREEN}Ваш баланс:{RESET} {balance}")
        input()
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def getInfo(data: dict) -> None:
    account = data['account']
    system('cls')
    try:
        prettyOut(f"{YELLOW}Выберите какую информацию вы хотите получить:{RESET}")

        actions = {
            1: enabledEstate,
            2: Advertisements,
            3: getSmartContractBalance,
            4: getAccBalance
        }
        try:
            choice = int(prettyInput(
                ['Доступная недвижимость', 'Текущие объявления о продаже', 'Баланс на смартконтракте',
                 'Баланс аккаунта']))
            if choice < 1 or choice > 4:
                prettyOut(f"{RED}Выберите число от 1 до 4!{RESET}")
                sleep(0.5)
            else:
                actions[choice](account)
        except ValueError:
            prettyOut(f"{RED}Недопустимое значение{RESET}")
    except Exception as e:
        prettyOut(f"\n{RED}Что-то пошло не так, {RESET}ошибка {e}")
        input()


def closeProgramm(data: dict) -> None:
    data['account'] = ""


def pay(data: dict):
    account = data["account"]
    try:
        prettyOut(f"{YELLOW}Введите кол-во WEI для пополнения:{RESET} ")
        value = int(input())
        if not value:
            return
        if value <= 0:
            print("Значение должно быть больше нуля")
            return
        tx_hash = contract.functions.pay().transact({
            'from': account,
            'value': value
        })

        prettyOut(f"Операция успешно выполнена. Хэш операции: {tx_hash}")
        input()
    except Exception as e:
        print(f"Ошибка пополнения: {e}")


def main():
    data = {'account': ""}
    while True:
        system('cls')
        if data['account'] == "":
            prettyOut(f'{YELLOW}Добро пожаловать в систему!{RESET}\n')
            try:
                choice = int(prettyInput(['Регистрация', 'Авторизация', 'Выйти']))
                match choice:
                    case 1:
                        register()
                    case 2:
                        data['account'] = login()
                    case 3:
                        prettyOut(f"{RED}\n\n\nВыход из программы!")
                        quit()
                    case _:
                        print("Выберите 1 или 2!")
            except ValueError:
                prettyOut(f"{RED}Недопустимое значение{RESET}")
        else:
            system('cls')
            actions = {
                1: createEstate,
                2: createAd,
                3: changeEstateStatus,
                4: changeAdStatus,
                5: byEstate,
                6: withdraw,
                7: getInfo,
                8: pay,
                9: closeProgramm
            }
            try:
                choice = int(prettyInput(['Создать недвижимость', 'Создать объявление', 'Изменить статус недвижимости',
                                          'Изменить статус объявления', 'Покупка недвижимости', 'Вывод средств',
                                          'Получение информации', 'Пополнить аккаунт', 'Выход']))
                if choice < 1 or choice > 9:
                    prettyOut(f"{RED}Выберите число от 1 до 9!{RESET}")
                    sleep(0.5)
                else:
                    actions[choice](data)
            except ValueError:
                prettyOut(f"{RED}Недопустимое значение{RESET}")


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        prettyOut(f"{RED}\n\n\nВыход из программы!")
        quit()
