from colors import *
from time import sleep


def prettyInput(args: list) -> None:
    print(f"{YELLOW}Выберите: {RESET}")
    index = 1
    for item in args:
        prettyOut(f"\t{GREEN}{index}.{RESET} {item}")
        index += 1
    return input(f"\n{BLUE}Введите номер пункта: {RESET}")


def prettyOut(item: str) -> None:
    for char in item:
        print(char, end="", flush=True)
        sleep(0.01)
    print()